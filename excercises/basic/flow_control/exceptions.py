
print("Total number of exceptions in Python 3.6" + len([s for s in dir(__import__('builtins')) if s.endswith('Error')]))
"""
Quite a lot BUT - typically in most of cases we see in execution error stacks TypeError or ValueError. They are all
very generics!

Consider this - TypeError can be thrown for following situation - additional point to the person who will find nice 
correlation there:
1. Trying to "call" object which is not callable
2. Called a function with too few arguments
3. Called a function with too many arguments
4. Called a function and not supply mandatory parameters
5. Try to pass a keyword argument to a function that accepts none (and oposite situation - keyword is expected and 
user pass positional argument)
...

Other common exceptions are:
AtrributeError - try of getting non-existent attribute
IndexError - OUB
KeyError - OUB but for dicts (when "indexes" are strings)


>>> abs()
TypeError: abs() takes exactly one argument (0 given)
>>> min()
TypeError: min expected 1 arguments, got 0
>>> map()
TypeError: map() requires at least two args
>>> open()
TypeError: Required argument 'name' (pos 1) not found
>>> object(42)
TypeError: object.__new__() takes no parameters

"""

# ignore exception
try:
    1 / 0
except ZeroDivisionError as e:
    print(e)


# re-raise exception
try:
    1 / 0
except ZeroDivisionError as e:
    print("Re-raise exception %s" % e)
    raise e


class MyCustomError(Exception):
    pass


raise MyCustomError("ERROR!!!")


