

class OnlyOne(object):
    """
        @docstring
    """
    class __Singleton(object):
        _singleton_value = None

        def __init__(self, arg):
            self._singleton_value = arg

        def __str__(self):
            return repr(self) + self._singleton_value

    instance = None

    def __init__(self, mandatory_parameter, *args, **kwargs):
        super(OnlyOne, self)
        if not OnlyOne.instance:
            instance = OnlyOne.__Singleton(mandatory_parameter)
        else:
            OnlyOne.instance._singleton_value = mandatory_parameter

    def __getattr__(self, attr_name):
        return getattr(self.instance, attr_name)

    def print_args(*args):
        for arg in args:
            print("Argument {0} has value {1}".format(args.index(arg), arg))
        # or
        for entity in enumerate(args):
            print("Argument {0} has value {1}".format(entity[0], entity[1]))


setup_object = OnlyOne("")
