from excercises.basic import setup_object

"""
Kiedy próbujemy zaimportować moduł, interpreter sprawdza wbudowane moduły, następnie szuka plików o nazwie
<moduł>.py w folderach znajdujących się na sys.path (PYTHONPATH)
"""

setup_object.print_args("example")

# HINT: advanced one
__import__('excercises.basic.bb_importing_packages').basic.bb_importing_packages.import_string

