"""
@docstring Empty
@author Paweł Tarsa

Napisz funkcje, ktora bedzie na wejsciu przyjmowala ciag znakow,
a na wyjsciu zwracala ciag znakow z podmienionymi znakami:
- a na 3
- o na 0
- u na 8
- Oraz wszystkie duze litery zamienione na male.
Zwroc nowy string.
Klasa str posiada odpowiednie funkcje do zamiany znakow. Wyszukaj je za pomoca googla, badz pythonowego helpa.
"""


def change_chars(givenString):
    givenString = givenString.lower()
    givenString = givenString.replace('a', '3')
    givenString = givenString.replace('o', '0')
    givenString = givenString.replace('u', '8')
    return givenString


def change_chars2(givenString):
    return givenString.lower().replace('a', '3').replace('o', '0').replace('u', '8')


mineString = 'jgsdfgHGJHSGDJKAHG23768bBSHGdadsasda'
print(mineString)
newString = change_chars(mineString)
print(newString)
newString2 = change_chars2(mineString)
print(newString2)
