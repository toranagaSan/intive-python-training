"""
@docstring Ref.: https://realpython.com/python-f-strings/
@author Paweł Tarsa

Excercise - have a look on the below examples and make an comparision of all of those ways of formatting the strings.
use for it timeit.timeit() method.
"""



# Example
name = "Paweł"
age = 32

print(f"Hello, {name}. You are {age}")


# Example

def to_lowercase(input_string):
    return input_string.lower()


print(F"{to_lowercase(name)} is funny")


# Example
class Comedian:
    def __init__(self, first_name, last_name, age):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age

    def __str__(self):
        return f"{self.first_name} {self.last_name} is {self.age}."

    def __repr__(self):
        return f"{self.first_name} {self.last_name} is {self.age}. Surprise!"


trainer = Comedian("Paweł", "Tarsa", 29)
print(f"{trainer}")