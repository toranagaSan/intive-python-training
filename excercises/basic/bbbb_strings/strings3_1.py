"""
@docstring Empty
@author Paweł Tarsa

Zaimplementuj program, ktory bedzie przyjmowal na wejscie ciag znakow a na wyjsciu zwracal,
ktory ze znakow wystepuje w ciagu najwieksza liczbe razy.

* Jesli najwieksza liczbe razy w ciagu bedzie wystepowac wiecej niz 1 znak zwroc wszystkie wraz z liczba ich wystapien
i procentowym
"""


def charsCounts(givenString):
    chars = set(givenString)
    return {k: givenString.count(k) for k in chars}


def biggestCount(charsCountDict):
    biggest = max(charsCountDict.values())
    chars = []
    for k, v in charsCountDict.items():
        if v == biggest:
            chars.append(k)
    return chars, biggest


def countPercentage(givenString, chars, value):
    strLength = len(givenString)
    charsLength = len(chars) * value
    percentage = (charsLength / strLength)*100
    print("Percentage: ", percentage)

mineString = 'jhjhgagoiqeurmxcnbzhgfquery/ffnmasv vmnakjyriqwbxczvjdfyaoiuyvzxm cvmnwelkwef'
charsCountsDict = charsCounts(mineString)
print(charsCountsDict)
biggestChars, biggestValue = biggestCount(charsCountsDict)
print(biggestChars, ", ", biggestValue)
countPercentage(mineString, biggestChars, biggestValue)