"""
@docstring Empty
@author Paweł Tarsa


Do poprzedniego zadania dodaj sprawdzania:
- Zamiana znakow powinna nastapic tylko jesli podana do funkcji wartosc jest stringiem
- Zamiana znakow odpowiednio powinna nastapic tylko jesli dany znak wystepuje w ciagu
"""


def changeChars(givenString):
    if type(givenString) == str:
        givenString = givenString.lower()
        if givenString.find('a'):
            givenString = givenString.replace('a', '3')
        if givenString.count('o'):
            givenString = givenString.replace('o', '0')
        if 'u' in givenString:
            givenString = givenString.replace('u', '8')
        return givenString
    else:
        print('Podano inny typ danych niz string')
        return False

mine_string = 'jgsdfgHGJHSGDJKAHG23768bBSHGdadsasda'
print(mine_string)
newString = changeChars(mine_string)
print(newString)
