"""
@docstring Empty
@author Paweł Tarsa

Zaimplementuj program, ktory bedzie przyjmowal na wejscie ciag znakow a na wyjsciu zwracal,
ktory ze znakow wystepuje w ciagu najwieksza liczbe razy.

* Jesli najwieksza liczbe razy w ciagu bedzie wystepowac wiecej niz 1 znak zwroc wszystkie wraz z liczba ich wystapien
i procentowym
"""


def charsCounts(givenString):
    charsDict = {}
    for char in givenString:
        if char in charsDict.keys():
            charsDict[char] += 1
        else:
            charsDict[char] = 1
    return charsDict


def biggestCount(charsCountDict):
    biggest = max(charsCountDict.values())
    print(biggest)
    for k, v in charsCountDict.items():
        if v == biggest:
            return k

chars = 'jhjhgagoiqeurmxcnbzhgfquery/ffnmasv vmnakjyriqwbxczvjdfyaoiuyvzxm cvmnwelkwef'
charsCountsDict = charsCounts(chars)
print(charsCountsDict)
biggestChar = biggestCount(charsCountsDict)
print(biggestChar)
