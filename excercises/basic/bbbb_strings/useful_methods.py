"""
@docstring Empty
@author Paweł Tarsa
"""

count()
strip()
split()
upper()
lower()
replace()

# repr() vs str()
import datetime

t = datetime.datetime.now()
str(t)
repr(t)
eval(str(t))
eval(repr(t))
