"""
Uywamy exec do dynamicznego egzekwowania kodu. W odroznieniu do eval (ewaluacja) dostarczony kod jest procesowany
w "standardowy" sposob tj. jezeli argumentem jest string, jest on najpierw parsowany, nastepnie jezeli nie ma tam bledow
- egzekwowany. Nie jest to pelnoprawne wstrzykiwanie kodu - w ciele definiowanych funkcji-stringow nie mozemy uzywac
return oraz yield


NOTE: it can be extremally risky if You will get too much controle to user (code INJECTIONS!!!!)
"""

print(dir(exec))

help(exec)

code_to_inject = 'a=7\nprint("a*17=",a*17)'  # do not forget
exec(code_to_inject)

exec('[(x**2) for x in range(7)]')

# example injection
import os

code_to_inject = input('What would you like to do today?')
# type print(os.listdir())
exec(code_to_inject)  # passing input object

"""
simple safe solution - execute dir() without any argument and execute - You will have an info what can be potentially 
available for hacker (user)
"""
