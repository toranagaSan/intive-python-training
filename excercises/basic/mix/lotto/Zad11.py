"""
Stworz nowy plik, ktory w nazwie bedzie zawieral timestampa.
Wpisz do pliku losowa liczbe linii z 6 liczbami z zakresu 1-49
Otworz plik ponownie do odczytu, zczytaj z niego wszystkie wartosci w tym:
- Zczytaj z pliku wystapienia kazdej z liczb
- W danej linii nie moga byc dwie te same liczby, takie losowanie jest uznane za niewazne
- Zwroc 6 liczb, ktore wystepowaly najczesciej
Podziel odpowiednio program na funkcje oraz dodaj obsluge wyjatkow.

Zadanie do domu:
Sprobuj rozszerzyc program o zczytywanie wartosci ze wszystkich plikow znajdujacych sie w danym folderze.
Sprobuj napisac powyzszy program obiektowo.
"""
import time
from random import randint


def generateFileName():
    timestamp = time.time()
    return str(timestamp)[:str(timestamp).index('.')] + ".txt"


def fillFileWithRandomData(outputFileName):
    linesCount = randint(30, 50)
    with open(outputFileName, 'w') as fileHandler:
        for line in range(0, linesCount):
            fileHandler.write(', '.join([str(randint(1, 50)) for ele in range(0, 6)]))
            if line != linesCount-1:
                fileHandler.write('\n')


def cleanData(data):
    cleanedData = []
    for ele in data:
        tmpData = ele.split(', ')
        if '\n' in tmpData[-1]:
            tmpData[-1] = tmpData[-1].replace('\n', '')
        cleanedData.append(tmpData)
    return cleanedData


def readDataFromFile(inputFileName):
    with open(inputFileName, 'r') as fileHandler:
        fileLines = fileHandler.readlines()
    return cleanData(fileLines)


def deleteInvalidDraw(data):
    for ele in data:
        tmpData = set(ele)
        if len(tmpData) < 6:
            print('draw deleted: {0}'.format(ele))
            data.remove(ele)
    return data


def countAllNumbers(data):
    counts = {}
    for draw in data:
        for number in draw:
            if number in counts.keys():
                counts[number] += 1
            else:
                counts[number] = 1
    return counts


def getMostOccurringNumbers(data):
    numbersLeft = 6
    lottoNumbers = []
    for numIndex in range(0, 6):
        maxValue = max(data.values())
        for key in data.keys():
            if data[key] == maxValue:
                lottoNumbers.append([int(key), maxValue])
                numbersLeft -= 1
            if numbersLeft == 0:
                return lottoNumbers


fileName = generateFileName()
fillFileWithRandomData(fileName)
lottoData = readDataFromFile(fileName)
print('Draws: {0}'.format(len(lottoData)))
lottoData = deleteInvalidDraw(lottoData)
print('Draws: {0}'.format(len(lottoData)))
numberCounts = countAllNumbers(lottoData)
numbers = getMostOccurringNumbers(numberCounts)
print(numbers)
