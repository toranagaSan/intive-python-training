"""
Stworz nowy plik, ktory w nazwie bedzie zawieral timestampa.
Wpisz do pliku losowa liczbe linii z 6 liczbami z zakresu 1-49
Otworz plik ponownie do odczytu, zczytaj z niego wszystkie wartosci w tym:
- Zczytaj z pliku wystapienia kazdej z liczb
- W danej linii nie moga byc dwie te same liczby, takie losowanie jest uznane za niewazne
- Zwroc 6 liczb, ktore wystepowaly najczesciej
Podziel odpowiednio program na funkcje oraz dodaj obsluge wyjatkow.

Zadanie do domu:
Sprobuj rozszerzyc program o zczytywanie wartosci ze wszystkich plikow znajdujacych sie w danym folderze.
Sprobuj napisac powyzszy program obiektowo.
"""
import os


def readAllData():
    files = [f for f in os.listdir('.') if os.path.isfile(f) and f.endswith('.txt')]
    allData = []
    for f in files:
        allData.extend(readDataFromFile(f))
    return allData


def cleanData(data):
    cleanedData = []
    for ele in data:
        tmpData = ele.split(', ')
        if '\n' in tmpData[-1]:
            tmpData[-1] = tmpData[-1].replace('\n', '')
        cleanedData.append(tmpData)
    return cleanedData


def readDataFromFile(fileName):
    with open(fileName, 'r') as fileHandler:
        fileLines = fileHandler.readlines()
    return cleanData(fileLines)


def deleteInvalidDraw(data):
    for ele in data:
        tmpData = set(ele)
        if len(tmpData) < 6:
            data.remove(ele)
    return data


def countAllNumbers(data):
    counts = {}
    for draw in data:
        for number in draw:
            if number in counts.keys():
                counts[number] += 1
            else:
                counts[number] = 1
    return counts


def getMostOccurringNumbers(data):
    numbersLeft = 6
    lottonNumbers = []
    for numIndex in range(0, 6):
        maxValue = max(data.values())
        for key in data.keys():
            if data[key] == maxValue:
                lottonNumbers.append([int(key), maxValue])
                numbersLeft -= 1
            if numbersLeft == 0:
                return lottonNumbers


lottoData = readAllData()
print('Draws before cleaning: {0}'.format(len(lottoData)))
lottoData = deleteInvalidDraw(lottoData)
print('Draws after cleaning: {0}'.format(len(lottoData)))
numberCounts = countAllNumbers(lottoData)
numbers = getMostOccurringNumbers(numberCounts)
print(numbers)
