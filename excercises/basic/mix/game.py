"""
@docstring Empty
@author Paweł Tarsa

Zaimplementuj gre -Kamien, papier i nozyczki-. Zasady:
- Mamy dwoch graczy
- Kazdy z graczy wybiera jedna z opcji (jest pytany o nia przez program)
- Kamien pokonuje nozyczki
- Nozyczki pokonuja papier
- Papier pokonuje kamien
Program ma wyswietlac, ktory z graczy wygral partie.
Jesli kazdy z graczy wybierze to samo, powinien zostac ogloszony remis.
Jesli ktorys z graczy wybierze zla opcje, powinien zostac poinformowany o blednym wyborze
i zapytany jeszcze raz o wybor.
* Rozwin program o dodanie opcji grania, az ktorys z graczy nie wygra co najmniej 3 razy.
"""


choices = ['k', 'p', 'n']


def askForChoice():
    while True:
        choice = input('What you choose k/p/n? ')
        if choice in choices:
            return choice
        print('Wrong choice, try again.')


def game():
    print('Choice for player1')
    player1 = askForChoice()
    print('Choice for player2')
    player2 = askForChoice()
    if player1 == player2:
        print('Tie!')
    elif player1 == 'k':
        if player2 == 'p':
            print('player2 won!')
        else:
            print('player1 won!')
    elif player1 == 'p':
        if player2 == 'k':
            print('player1 won!')
        else:
            print('player2 won!')
    elif player1 == 'n':
        if player2 == 'p':
            print('player1 won!')
        else:
            print('player2 won!')

game()
