"""
@docstring Go to subprocess documentation and read it.
@author Paweł Tarsa
"""
import subprocess

# run a command, wait until finish and get return code (call return object CompletedProcess). We can pass command also
# like [name of command, argument1, argument2]
subprocess.call('exit 1', shell=True)

subprocess.call('dir', shell=True)

"""
subprocess.run(args, *, stdin=None, input=None, stdout=None, stderr=None, capture_output=False, shell=False, cwd=None, timeout=None, check=False, encoding=None, errors=None, text=None, env=None)
return CompletedProcess
"""

"""
check_output make possible to validate result of executed command
"""
subprocess.check_output(["echo", "Hello World!"], shell=True)

"""
Popen.communicate make possible to send input data to process. It return a tuple like (stdout_data, stderr_data)
"""
_popen = subprocess.Popen(["echo", "hello world"], stdout=subprocess.PIPE, shell=True)
_stdout, _stderr = _popen.communicate()
print("*\n" * 2)
print(_stdout)

p = subprocess.Popen(['python', '-u',  # file_to_run,
                      '-s', '-g', '-i', ],  # input_file],
                     universal_newlines=True,
                     stdout=subprocess.PIPE)
# p.communicate()
out = p.stdout.readline().rstrip()
