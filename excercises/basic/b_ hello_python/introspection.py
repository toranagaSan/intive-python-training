"""
@docstring This is docstring
@author Paweł Tarsa
"""
import sys

# this is comment
# dir() summarized informations: constants, functions, methods
dir(sys)
# help() - detailed informations
help(sys)
# getting info about modules in package
print(sys.modules)
# getting info about path env
print(sys.path)

# type()
a = int(5)
type(a)
