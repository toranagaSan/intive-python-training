"""
@docstring Program which ask for username and print it out
@author Paweł Tarsa
"""

# there is one widely accepted principle in python: It’s better to ask for forgiveness rather than permission.
# todo http://xion.org.pl/2013/06/16/ask-for-forgiveness-not-permission/
import this

print("Hello %s" % input("Pass Your name dear Sir/Madame: "))
print("Hi, {0}!".format(input("Pass Your name dear Sir/Madame: ")))

# in java
"""
import java.util.*;
import java.lang.*;
import java.io.*;

/class Ideone
{
	public static void main (String[] args) throws java.lang.Exception
	{
		Scanner inputScanner = new Scanner(System.in);
		System.out.println("Please type Your name, dear Sir/Madame:");
		System.out.println("Hello " + s.next());
	}
}
"""
