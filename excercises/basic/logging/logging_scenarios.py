"""
@docstring Refers to logging module documentation: @link https://docs.python.org/3/howto/logging.html
@author Paweł Tarsa
"""
import logging

logging.basicConfig(filename="debug.log",
                    format="%(asctime)s %(levelname)s:%(message)s",
                    datefmt="%m/%d/%Y %I:%M:%S %p",
                    level=logging.DEBUG)  # it means all higher ranked than DEBUG

logging.debug("message")
logging.info("message")
logging.warning("message")
logging.error("message")
logging.critical("message")

# nice hack - redirecting output streams
import sys  # import anytime:)

new_output = open('error.log', 'w')
sys.stderr = new_output
raise Exception('this is an error')
fsock.close()

# hint - always use named loggers (logging.getLogger(__name__)
