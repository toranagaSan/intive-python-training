"""
@docstring Empty
@author Paweł Tarsa
"""

"""
@note dict comprehension example
"""
example_object = {x: x for x in range(10)}
print(example_object)
