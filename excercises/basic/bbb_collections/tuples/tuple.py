"""
@docstring Empty
@author Paweł Tarsa
"""

# tuple packing

_tuple = 1, 2, 3, 4, 5, 6
assert isinstance(_tuple, tuple), "To nieprawda, wcale nie stworzyłeś krotki"

_tuple = ('x',)
assert isinstance(_tuple, tuple), "To nieprawda, wcale nie stworzyłeś krotki"
assert _tuple[0] != 1
assert _tuple.__len__() == 1

# tuple unpacking - IMPORTANT: there need to be exact number of variables on the left side
_tuple = 1, 2, 3, 4, 5, 6
a, b, c, d, e, f = _tuple
assert a == 1 and f == 6

# tuple slicing
assert len(_tuple[1:3]) == 2

# negative indexing
assert _tuple[-1] == 6

# tuples are immutable
try:
    _tuple[1] = 'some other value'
except TypeError as e:
    import sys

    sys.stderr.print(e)

# useful functions
len()
max()
min()
sum()
any()
all()
sorted()
tuple([])
_tuple.index()
_tuple.count()
print(("a",) + (10,))
