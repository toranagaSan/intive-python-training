"""
@docstring Empty
@author Paweł Tarsa

Stworz liste zawierajaca jako elementy slownik skladajacy sie z danych takich jak:
- Imie
- Nazwisko
- Nr telefonu
A potem wypisz:
- Wartosc imienia pierwszego slownika w liscie
- Wartosc telefonu ostatniego slownika w liscie
- Zmien nazwisko w drugim slowniku w liscie a potem wypisz je pobierajac ze slownika

Stworz liste, ktora bedzie zawierala obiekty roznego typu (int, string, dict, other list, tuple).

Usefull functions
_list.append()
_list.insert()
_list.clear()
_list.index()
_list.remove()
_list.count()
_list.pop()
_list.sort()
_list.reverse()

"""
anna = {'forename': 'anna', 'surname': 'kowalska', 'phoneNumber': 123123123}
jacek = {'forename': 'jacek', 'surname': 'nowakowski', 'phoneNumber': 345345345}
adam = {'forename': 'adam', 'surname': 'rudy', 'phoneNumber': 987988709}
personel = [anna, jacek, adam]
print(personel[0]['forename'])
print(personel[-1]['surname'])
personel[1]['surname'] = 'bialy'
print(personel[1])  # caly slownik
print(personel[1]['surname'])  # samo nazwisko
