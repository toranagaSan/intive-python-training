"""
Counter
DefaultDict
OrderedDict
NamedTuple
"""

from collections import Counter, defaultdict, OrderedDict, namedtuple

issubclass(Counter, dict) and issubclass(defaultdict, dict) and issubclass(OrderedDict, dict)

_counter = Counter("String with a lot of content")
print(_counter)

_counter.update('aaaaaa')
print(_counter)

print(_counter.most_common())

"""
defaultdict
"""
_default_dict = defaultdict(lambda: 99)
_default_dict['Pawel\'s age'] = 33
_default_dict['Grzegorz age']

_default_dict.__missing__('Grzegorz age')  # checking the default value using missing

"""
OrderedDict - just remember order in which user pass pairs
"""
_ordered_dict = OrderedDict()

# _ordered_dict.move_to_end(key, last=False)  #move pair to the end or to the front
# _ordered_dict.popitem(key)  #remove last and get

"""
namedtuple - python version of enum?
"""

colors = namedtuple('colors', 'r g b')
red = colors(r=255, g=0, b=0)
red.r
red.g
red[0]
# U can convert to dict - because You've gave names to inside parameters
red.as_dict()
# U can convert list to namedtuple
colors._make(['254', '1', '255'])
# U can check what fields are inside namedtuple
colors._fields
