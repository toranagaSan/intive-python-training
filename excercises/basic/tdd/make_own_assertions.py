# http://xion.org.pl/2014/02/09/specializing-testcase-assertraises/
from contextlib import contextmanager
from unittest import TestCase


def raise_type_error3():
    def method_with_one_mandatory_parameter(mandatory_parameter):
        pass

    method_with_one_mandatory_parameter()


def raise_type_error():
    raise TypeError("NoneType")


# python 3.x
class Foo3(TestCase):
    def test_none(self):
        self.assertRaises(TypeError, raise_type_error3)


# python 2.7

class MyTestCase(TestCase):
    @contextmanager
    def assertNoneTypeException(self):
        with self.assertRaises(TypeError) as r:
            yield r
        self.assertIn("NoneType", str(r.exception))


class Foo(MyTestCase):
    def test_none(self):
        with self.assertNoneTypeException():
            raise_type_error()


# main does not need to be there as we inherited by unittest TestCase - what means that there is a runner
if __name__ == '__main__':
    Foo3().test_none()
