"""
@docstring Despite of below examples etc. please have a look also on hooks in context of python

@author Paweł Tarsa
"""

import pytest


def my_func():
    raise ValueError("Exception 123 raised")


def test_zero_division():
    with pytest.raises(ZeroDivisionError, message="Expecting ZeroDivisionError"):
        1 / 0


def test_match():
    with pytest.raises(ValueError, match=r'.* 123 .*'):
        my_func()


@pytest.mark.xfail(raises=IndexError)
def test_f():
    a = []
    a[1]


