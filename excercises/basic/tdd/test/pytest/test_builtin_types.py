import excercises.basic.tdd.builtin_types as built_types

def f():
    return 3


def test_function():
    assert f() == 3, "The expected value which returns the function f is 4!!!"


def test_2_points_precision_rounding():
    assert built_types.round_2_points_precision("3.2121") == 3.21
