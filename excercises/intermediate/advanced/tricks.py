"""
@docstring Empty
@author Paweł Tarsa
"""

# unpacking
a, *b, c = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print("a: {a}, b: {b}, c: {c}".format(a=a, b=b, c=c))

# negative indexes
print("The last one %s" % b[-1])

# list slicing (negative slicing as well)
print("Slice of list %s" % b[1:4])

# reversing list
print("Reversed list (get element one by one by from back) %s" % b[::-1])

# Iterating over dictionaries key and value pairs (dict.iteritems) - like Entry in java
for k, v in enumerate(b):
    print("Key: %s value: %s" % (k, v))

# zipping and unzipping list and iterables
zipped_lists = zip([a, c], b)
[print(_zip) for _zip in zipped_lists]

# https://sahandsaba.com/thirty-python-language-features-and-tricks-you-may-not-know.html

# dictionary comprehension
{x: 'A' + str(x) for x in range(10)}

