Starting point: https://www.youtube.com/watch?v=7VJaprmuHcw


# python3-training

## Requirements

* Python 3.6+: https://www.python.org/downloads/
* IDE with Python support (PyCharm recommended): https://www.jetbrains.com/pycharm/download/

## Exercises

Exercises are available in `exercises` package.
Run test to check the results:

    $ cd code_examples
    $ python3 -m unittest

To run single test append test reference name:

    $ python3 -m unittest excercises.test.test_word_count.OperatorsTest.test_get_word_count_dict
    
Topic to review:
https://softwareengineering.stackexchange.com/questions/314808/why-variables-in-python-are-different-from-other-programming-languages
https://selenium-python.readthedocs.io/getting-started.html
https://www.cheatography.com/davechild/cheat-sheets/python/
http://book.pythontips.com/en/latest/index.html
https://pl.wikibooks.org/wiki/Zanurkuj_w_Pythonie
http://xion.org.pl/category/it/coding/page/5/




