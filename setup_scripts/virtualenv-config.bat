// NOTE run as an administrator

cd ../

$host.ui.RawUI.WindowTitle = "virtualenv-setup"
pip install virtualenv
virtualenv virtualenv-intive   //--no-site-packages (with no globally installed packages) useful when You want to keep clean environment

// for unix like users: source virtualenv-intive/Scripts/activate  //it just change shell environments to point to newly created python instance. NOTE it does not update PATH environment variable,
 //so if You use subprocess to start another one python process for example - then it will probably run on different python's binary version

Set-ExecutionPolicy RemoteSigned  // relaxing system's execution policy to AllSigned - meaning that all scripts which will be executed need to be digitally signed
./virtualenv-intive/Scripts/activate

Write "selenium>=3.141.0`npytest>=3.3.2`ncoverage>=4.5`nipython>=7.2`npycodestyle>=2.4`nrequests>=2.20" | Out-File requirements.txt

pip install -r requirements.txt

//propmpt has changed
deactivate
