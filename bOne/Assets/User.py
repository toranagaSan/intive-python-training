class User(dict):

    def __init__(self, **kwargs):
        """
        :param kwargs:
        Suggested parameters:
            - login
            - password
            - member
            - fullname
        """
        for key, val in kwargs.items():
            self[key] = val

    def __getattr__(self, item):
        if item in self.keys():
            return self[item]
        else:
            return super(User, self).__getattribute__(item)


if __name__ == '__main__':
    u = User(login='p.tarsa', password="Test123456@", img='example_img', member='BUERGEL_M')
    assert u.login == 'p.tarsa'
    assert u['login'] == 'p.tarsa'
    assert u['password'] == 'Test12345678@'
    assert u['img'] == 'example_img'
    assert u['member'] == 'BUERGEL_M'
