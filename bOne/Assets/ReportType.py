from enum import Enum


class ReportType(Enum):
    ConCheck = 'ConCheckButton'
    ConCheckPlusIdent = 'ConCheckPlusIdentButton'
    NegativeCheck = 'NegativeCheckButton'
    NegativeCheckHard = 'NegativeCheckHardButton'
    RiskCheckBusiness = 'RiskCheckBusinessButton'
    RiskCheckBusinessCrawling = 'RiskCheckBusinessCrawlingButton'
    RiskCheckBusinessPlusCrawling = 'RiskCheckBusinessPlusButton'
