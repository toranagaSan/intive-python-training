selenium>=3.141.0
pytest>=3.3.2
coverage>=4.5
pycodestyle>=2.4
requests>=2.20
pyyaml>=3.13
parameterized>=0.7.0