import logging
from Pages.BasePage import BasePage
from Pages.Login.LoginPageObjects import LoginPageObjects


class LoginPage(BasePage):

    def login(self, user):
        """
        Return reference to search page because it redirect user after login to search page
        :param user:
        :return:
        """
        logging.debug(f'logging into Bone with credentials: {user.login} // {user.password}')
        self.get_element(LoginPageObjects.UserInput).clear()
        self.get_element(LoginPageObjects.UserInput).send_keys(user.login)
        self.get_element(LoginPageObjects.PasswordInput).clear()
        self.get_element(LoginPageObjects.PasswordInput).send_keys(user.password)
        self.get_element(LoginPageObjects.SignInButton).click()
        from Pages.Search.SearchPage import SearchPage
        return SearchPage(self._driver).wait_for_opening_and_get()

    def login_with_valid_user(self, user):
        from Pages.Search.SearchPage import SearchPage
        self.login(user)
        return SearchPage(self._driver)

    def login_with_invalid_user(self, user):
        self.login(user)
        self.wait_for_visibility_of_element_located(LoginPageObjects.LoginErrorMessage)
        _message_text = self.get_element(LoginPageObjects.LoginErrorMessage).text
        logging.debug(f"Error text value: {_message_text}")
        return _message_text

    # todo to do konca nie chce wspolpracowac
    def change_language_to_english(self):
        self.get_element(LoginPageObjects.EnglishLanguageBtn)
        self.wait_till_javascript_ends()

    def change_language_to_german(self):
        self.get_element(LoginPageObjects.GermanLanguageBtn)




