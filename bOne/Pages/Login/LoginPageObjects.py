from selenium.webdriver.common.by import By


class LoginPageObjects(object):
    UserInput = (By.XPATH, '//input[@type="text" and @name="username"]')
    PasswordInput = (By.XPATH, '//input[@type="password" and @name="password"]')
    SignInButton = (By.XPATH, '//button[@type="submit"]')
    LoginErrorMessage = (By.CLASS_NAME, 'errorText')
    GermanLanguageBtn = (By.XPATH, '/descendant::div[contains(@class, "lang_div ")][1]/a')
    EnglishLanguageBtn = (By.XPATH, '/descendant::div[contains(@class, "lang_div ")][2]/a')

    # todo all below will not work with German lang version
    MainLogo = (By.CLASS_NAME, 'main-logo')
    HomeLink = (By.PARTIAL_LINK_TEXT, 'HOME')
    ServicesLink = (By.PARTIAL_LINK_TEXT, 'SERVICES')
    AboutUsLink = (By.PARTIAL_LINK_TEXT, 'ABOUT US')
    ContactLink = (By.PARTIAL_LINK_TEXT, 'CONTACT')
    ForgotYourPasswordLink = (By.CLASS_NAME, 'registerLink')
    PrivacyPolicyLink = (By.PARTIAL_LINK_TEXT, 'PRIVACY POLICY')
    TermsConditionsLink = (By.PARTIAL_LINK_TEXT, 'TERMS & CONDITIONS')
    ImprintLink = (By.PARTIAL_LINK_TEXT, 'IMPRINT')
    NewsletterLink = (By.PARTIAL_LINK_TEXT, 'NEWSLETTER')
    TomLink = (By.LINK_TEXT, 'TOM')
