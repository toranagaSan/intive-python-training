import logging
from typing import Tuple

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from Helpers.ExpectedConditions.custom_expected_conditions import element_is_selectable


class BasePage(object):

    def __init__(self, driver: WebDriver):
        self._driver = driver

    def get_element(self, locator: Tuple[By, str]) -> WebElement:
        return self._driver.find_element(*locator)

    def get_elements(self, locator: Tuple[By, str]) -> WebElement:
        return self._driver.find_elements(*locator)

    def wait_and_click(self, locator: Tuple[By, str]) -> WebElement:
        self.wait_for_visibility_of_element_located(locator)
        _element = self.get_element(locator)
        if _element:
            _element.click()
        else:
            logging.warning('element is not clickable')


    def wait_till_javascript_ends(self, javascript="return jQuery.active == 0", wait_time=30):
        WebDriverWait(self._driver, wait_time).until(
            lambda driver: self._driver.execute_script(javascript),
            "Javascript finishing timeout"
        )

    def wait_for_opening_and_get(self):
        self.wait_till_javascript_ends('return document.readyState == "complete"')
        return self

    def wait_for_element_is_clickable(self, locator: Tuple[By, str]):
        self._wait_for(element_is_selectable, locator)

    def wait_for_presence_of_element_located(self, locator: Tuple[By, str]):
        self._wait_for(EC.presence_of_element_located, locator)

    def wait_for_visibility_of_element_located(self, locator: Tuple[By, str]):
        self._wait_for(EC.visibility_of_element_located, locator)

    def get_page_url(self):
        self.wait_till_javascript_ends()
        return self._driver.current_url

    def implicit_wait(self, time_to_wait=5):
        self._driver.implicitly_wait(time_to_wait)

    def _wait_for_element_and_get(self, locator: Tuple[By, str]) -> WebElement:
        self.wait_for_visibility_of_element_located(locator)
        return self.get_element(locator)


    def _wait_for(self, method, locator, timeout=60):
        WebDriverWait(self._driver, timeout).until(method(locator))


if __name__ == '__main__':
    from Helpers.Factories.WebdriverFactory import WebdriverFactory
    from Pages.Login.LoginPageObjects import LoginPageObjects

    bp = BasePage(WebdriverFactory.get_webdriver("chrome"))
    bp.wait_for_presence_of_element_located(LoginPageObjects.LoginErrorMessage)
