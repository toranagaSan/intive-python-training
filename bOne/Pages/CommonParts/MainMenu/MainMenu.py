import logging

from Pages.BasePage import BasePage
from Pages.CommonParts.MainMenu.MainMenuObjects import MainMenuObjects


class MainMenu(BasePage):

    def click_search_page_button(self):
        logging.info('Going to search page')
        self._driver.find_element(*MainMenuObjects.SearchPageButton).click()

    def click_workspace_page_button(self):
        logging.info('Going to workspace page')
        self._driver.find_element(*MainMenuObjects.WorkspacePageButton).click()

    def click_settings_page_button(self):
        logging.info('Going to settings page')
        self._driver.find_element(*MainMenuObjects.SettingsPageButton).click()

    def click_skyminder_page_button(self):
        logging.info('Going to skyminder page')
        self._driver.find_element(*MainMenuObjects.SkyMinderPageButton).click()

    def click_profile_info_graphic(self):
        logging.info('Going to profile page')
        self._driver.find_element(*MainMenuObjects.ProfilePageButton)

    def get_user_info_visible_in_main_menu(self):
        logging.info('Collecting user information')
        from Assets.User import User
        _user = User()
        for property_name, locator in MainMenuObjects.ProfileInformation.items():
            if property_name == 'img':
                continue
            self.wait_for_visibility_of_element_located(locator)
            _user[property_name] = self._driver.find_element(*locator).text
        logging.error(_user)
        return _user
