from selenium.webdriver.common.by import By


class MainMenuObjects(object):
    ProfilePageButton = (By.CLASS_NAME, "my_image_a  ")  # todo check if it works with no white chars
    SearchPageButton = (By.ID, 'menu_item_Search')
    WorkspacePageButton = (By.ID, 'menu_item_BoneReports')
    SettingsPageButton = (By.ID, 'menu_item_Settings')
    SkyMinderPageButton = (By.ID, 'menu_item_SkyMinder')

    # todo good to add IDs for below
    ProfileInformation = {
        # "img": (By.XPATH, '//a[contains(@class, "my_image_a")]/img[@class="my_image_medium"]'),
        "user_name": (By.XPATH, '//a[contains(@class, "my_image_a")]/div[@class="my_image_username"]'),
        "member": (By.XPATH, '//a[contains(@class, "my_image_a")]/div[last()-1]'),
        "id": (By.XPATH, '//a[contains(@class, "my_image_a")]/div[last()]'),
    }
