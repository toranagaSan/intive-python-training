from selenium.webdriver.common.by import By


class ActionMenuObjects(object):
    ActionMenuButton = (By.XPATH, '//i[@class="fa fa-bars"]')
    SearchHistoryButton = (By.XPATH, '//i[@class="fa fa-history"]/..')
    InvestigationHistoryButton = (By.XPATH, '//i[@class="fa fa-retweet"]/..')
    ExportButton = (By.XPATH, '//i[@class="fa fa-file-pdf-o"]/..')
    MarkAsReadButton = (By.XPATH, '//i[@class="fa fa-file-pdf-o"]/..')
    MarkAsUnreadButton = (By.XPATH, '//i[@class="fa fa-file-pdf-o"]/..')
