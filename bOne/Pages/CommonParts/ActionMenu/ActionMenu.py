from Pages.BasePage import BasePage
from Pages.CommonParts.ActionMenu.ActionMenuObjects import ActionMenuObjects as AM


class ActionMenu(BasePage):

    def open(self):
        self.get_element(AM.ActionMenuButton).click()
        # todo wait for visibility?

    def click_search_history_btn(self):
        pass
        # self._driver.find_element()

    def wait_for_action_menu_btn_visible(self):
        self.wait_for_visibility_of_element_located(AM.ActionMenuButton)
