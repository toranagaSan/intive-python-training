from Pages.BasePage import BasePage
from Pages.Report.ReportPageObjects import ReportPageObjects


class ReportPage(BasePage):

    def __init__(self, driver):
        super(ReportPage, self).__init__(driver)

    def is_report_page_loaded(self):
        return self._wait_for_element_and_get(ReportPageObjects.ReportPlaceholder).is_displayed()