from selenium.webdriver.common.by import By

from Assets.ReportType import ReportType


class AdvancedSearchObjects(object):
    AdvancedSearchWindow = (By.XPATH, '//div[contains(@class, "advance-search-window")]')
    ExactMatchInput = (By.CSS_SELECTOR, 'i[value="Exact match"]')
    NameInput = (By.CSS_SELECTOR, 'input[name="serId_name"]')
    StreetInput = (By.CSS_SELECTOR, 'input[name="serId_street"]')
    StreetNumberInput = (By.CSS_SELECTOR, 'input[name="serId_houseNo"]')
    ZipCodeInput = (By.CSS_SELECTOR, 'input[name="serId_zip"]')
    CityInput = (By.CSS_SELECTOR, 'input[name="serId_city"]')
    CountryInput = (By.CSS_SELECTOR, 'input[name="serId_country"]')
    DateOfBirthInput = (By.CSS_SELECTOR, 'input[name="serId_birthDate"]')
    TelephoneInput = (By.CSS_SELECTOR, 'input[name="serId_phone"]')
    WebsiteInput = (By.CSS_SELECTOR, 'input[name="website"]')
    IdentifierDropdown = (By.CSS_SELECTOR, 'input[name="serId_identifier#type"]')
    IdentifierInput = (By.CSS_SELECTOR, 'input[name="serId_identifier"]')
    # todo MaxResultsSlider = (By.CSS_SELECTOR, 'input[name=""]')
    # todo MinSimilaritySlider = (By.CSS_SELECTOR, 'input[name=""]')
    CompanyInput = (By.CSS_SELECTOR, 'i[value="Company"]')
    ManagerShareholderInput = (By.CSS_SELECTOR, 'i[value="Manager/Shareholder"]')
    ConsumerInput = (By.CSS_SELECTOR, 'i[value="Consumer"]')
    HeadOfficeInput = (By.CSS_SELECTOR, 'i[value="Head office"]')
    BranchInput = (By.CSS_SELECTOR, 'i[value="Branch"]')
    ActiveInput = (By.CSS_SELECTOR, 'i[value="Active"]')
    InactiveInput = (By.CSS_SELECTOR, 'i[value="Inactive"]')
    CloseButton = (By.CSS_SELECTOR, 'div[class*="x-tool-close"]')
    ClearButton = (By.CSS_SELECTOR, 'td[class="x-toolbar-right"]  button[class*="x-btn-gray-text"]')
    SearchButton = (By.CSS_SELECTOR, 'td[class="x-toolbar-right"]  button[class*="x-btn-orange-text"]')

    DpcSection = (By.XPATH, '//descendant::div[@class="row dpc-products x-component"]/div[1]')
    ConsumerSection = (By.CSS_SELECTOR, '//descendant::div[@class="row dpc-products x-component"]/div[2]')
    # Input = (By.CSS_SELECTOR, 'input[name=""]')
    # Input = (By.CSS_SELECTOR, 'input[name=""]')
    ConCheckPlusIdentButton = (By.XPATH, '//button[text()="ConCheck + Ident"]')
    ConCheckButton = (By.XPATH, '//button[text()="ConCheck"]')
    NegativeCheckButton = (By.XPATH, '//button[text()="NegativeCheck"]')
    NegativeCheckHardButton = (By.XPATH, '//button[text()="NegativeCheck Hard"]')
    RiskCheckBusinessButton = (By.XPATH, '//button[text()="RiskCheck Business"]')
    RiskCheckBusinessCrawlingButton = (By.XPATH, '//button[text()="RiskCheck Business + Crawling"]')
    RiskCheckBusinessPlusButton = (By.XPATH, '//button[text()="RiskCheck Business Plus + Crawling"]')

    @staticmethod
    def get_attribute_by_name(name):
        """
        Alternative approach
        def f(cls):
            return [i for i in cls.__dict__.keys() if i[:1] != '_']
        :param name:
        :return:
        """
        import inspect
        _static_attr = next(filter(lambda e: e[0] == name,
                                   inspect.getmembers(AdvancedSearchObjects, lambda x: not (inspect.iscoroutine(x)))),
                            None)
        if _static_attr:
            return _static_attr[1]
        return None


if __name__ == '__main__':
    assert AdvancedSearchObjects.get_attribute_by_name(ReportType.NegativeCheckHard.value) == (
    By.XPATH, '//button[text()="NegativeCheck Hard"]')
    assert AdvancedSearchObjects.get_attribute_by_name('ConCheckButton') == (By.XPATH, '//button[text()="ConCheck"]')
    assert AdvancedSearchObjects.get_attribute_by_name('Non_existing_element') == None
