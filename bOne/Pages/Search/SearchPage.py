from Pages.Search.AdvancedSearch import AdvancedSearch
import logging
from Pages.BasePage import BasePage
from Pages.Search.SearchPageObjects import SearchPageObjects
from Pages.CommonParts.ActionMenu.ActionMenu import ActionMenu


class SearchPage(BasePage):

    def __init__(self, driver):
        super(SearchPage, self).__init__(driver)
        # todo stworzyc osobny Part dla investygacji - tworzony w metodzie, nie w konstruktorze
        # todo 2 stworzyc osobny Part dla advanced search
        from Pages.CommonParts.MainMenu.MainMenu import MainMenu
        self._action_menu = ActionMenu(driver=self._driver)
        self._main_menu = MainMenu(driver=self._driver)

    def get_login_user_details(self):
        return self._main_menu.get_user_info_visible_in_main_menu()

    def wait_for_opening_and_get(self):
        # This is WA for page part-loading
        self._action_menu.wait_for_action_menu_btn_visible()
        return self

    def search(self, phrase):
        self._type_search_input(phrase)
        self._click_search_button()

    def open_action_menu(self):
        self._action_menu.open()

    def open_advanced_search(self) -> AdvancedSearch:
        self.get_element(SearchPageObjects.AdvancedSearchButton).click()
        return AdvancedSearch(self._driver)

    def collect_search_results(self):
        _collected_info = []
        self.wait_for_visibility_of_element_located(SearchPageObjects.SearchResultPlaceholder)
        _listing_results = self.get_elements(SearchPageObjects.SearchResultPlaceholder)
        logging.error(" tag value: " + str(len(_listing_results)))
        for _result in _listing_results:
            _collected_info.append(_result.text)
        return _collected_info

    def fill_up_advanced_search_form(self, input_data):
        return AdvancedSearch(self._driver).fill_up_form(input_data)

    def purchase_full_report(self, monitoring=False, plus=False):
        self.wait_for_visibility_of_element_located(SearchPageObjects.SearchResultPlaceholder)
        _company_record = self.get_elements(SearchPageObjects.SearchResultPlaceholder)[0]
        _company_record.find_element(*SearchPageObjects.SearchExpandRecordArrow).click()
        self.wait_and_click(SearchPageObjects.FullReportRadio)
        self._confirm(monitoring, plus)

    def purchase_full_report_gwg(self, monitoring=False, plus=False):
        self.wait_for_visibility_of_element_located(SearchPageObjects.SearchResultPlaceholder)
        _company = self.get_elements(SearchPageObjects.SearchResultPlaceholder)[0]
        _company.find_element(*SearchPageObjects.SearchExpandRecordArrow)
        self.wait_and_click(SearchPageObjects.FullReportGwGRadio)
        self._confirm(monitoring, plus)

    def purchase_credit_report(self, monitoring=False, plus=False):
        self.wait_for_visibility_of_element_located(SearchPageObjects.SearchResultPlaceholder)
        _company = self.get_elements(SearchPageObjects.SearchResultPlaceholder)[0]
        _company.find_element(*SearchPageObjects.SearchExpandRecordArrow)
        self.wait_and_click(SearchPageObjects.CreditReportRadio)
        self._confirm(monitoring, plus)

    def purchase_boni_check_report(self, monitoring=False, plus=False):
        self.wait_for_visibility_of_element_located(SearchPageObjects.SearchResultPlaceholder)
        _company = self.get_elements(SearchPageObjects.SearchResultPlaceholder)[0]
        _company.find_element(*SearchPageObjects.SearchExpandRecordArrow)
        self.wait_and_click(SearchPageObjects.BoniCheckCompactRadio).click()
        self._confirm(monitoring, plus)

    def purchase_finance_check_report(self, monitoring=False, plus=False):
        self.wait_for_visibility_of_element_located(SearchPageObjects.SearchResultPlaceholder)
        _company = self.get_elements(SearchPageObjects.SearchResultPlaceholder)[0]
        _company.find_element(*SearchPageObjects.SearchExpandRecordArrow)
        self.wait_and_click(SearchPageObjects.FinanceCheckRadio)
        self._confirm(monitoring, plus)

    def purchase_gwg_report(self, monitoring=False, plus=False):
        self.wait_for_visibility_of_element_located(SearchPageObjects.SearchResultPlaceholder)
        _company = self.get_elements(SearchPageObjects.SearchResultPlaceholder)[0]
        _company.find_element(*SearchPageObjects.SearchExpandRecordArrow)
        self.wait_and_click(SearchPageObjects.GwgReportRadio)
        self._confirm(monitoring, plus)

    def purchase_risk_check_report(self, monitoring=False, plus=False):
        self.wait_for_visibility_of_element_located(SearchPageObjects.SearchResultPlaceholder)
        _company = self.get_elements(SearchPageObjects.SearchResultPlaceholder)[0]
        _company.find_element(*SearchPageObjects.SearchExpandRecordArrow)
        self.wait_and_click(SearchPageObjects.RiskCheckBusinessRadio)
        self._confirm(monitoring, plus)

    def purchase_risk_check_plus_report(self, monitoring=False, plus=False):
        self.wait_for_visibility_of_element_located(SearchPageObjects.SearchResultPlaceholder)
        _company = self.get_elements(SearchPageObjects.SearchResultPlaceholder)[0]
        _company.find_element(*SearchPageObjects.SearchExpandRecordArrow)
        self.wait_and_click(SearchPageObjects.RiskCheckBusinessPlusRadio)
        self._confirm(monitoring, plus)

    def is_report_rendered(self):
        from Pages.Report.ReportPage import ReportPage
        return ReportPage(self._driver).is_report_page_loaded()

    def _confirm(self, monitoring=False, plus=False):
        self.wait_for_element_is_clickable(SearchPageObjects.ConfirmButton)
        self.get_element(SearchPageObjects.ConfirmButton).click()

        if monitoring:
            if plus:
                self.wait_and_click(SearchPageObjects.MonitoringPlusRadio)
            else:
                self.wait_and_click(SearchPageObjects.MonitoringRadio)

        self.wait_for_visibility_of_element_located(SearchPageObjects.PurchaseButton)
        self.get_element(SearchPageObjects.PurchaseButton).click()

    def _click_search_button(self):
        self._driver.find_element(*SearchPageObjects.SearchButton).click()

    def _type_search_input(self, user_input):
        logging.debug(f"searching for following input {user_input}")
        self._driver.find_element(*SearchPageObjects.SearchInput).clear()
        self._driver.find_element(*SearchPageObjects.SearchInput).send_keys(user_input)

    def _click_investigation_button(self):
        logging.debug("opening investigation window")
