from typing import List, Tuple, Dict

from selenium.webdriver.common.by import By

from Assets.ReportType import ReportType
from Pages.BasePage import BasePage
from Pages.Search.AdvancedSearchObjects import AdvancedSearchObjects


class AdvancedSearch(BasePage):

    def __init__(self, driver):
        self._driver = driver
        self.wait_for_visibility_of_element_located(AdvancedSearchObjects.AdvancedSearchWindow)

    def are_elements_visible(self, elements_list: List[Tuple[By, str]]):
        for _element in elements_list:
            self.wait_for_visibility_of_element_located(_element)
        return True

    def fill_up_form(self, input_data: Dict[Tuple[By, str], str]):
        for _locator, _value in input_data.items():
            self.wait_for_visibility_of_element_located(_locator)
            self.get_element(_locator).clear()
            self.get_element(_locator).send_keys(_value)
        return self

    def choose_consumer_search_type(self):
        self.wait_and_click(AdvancedSearchObjects.ConsumerInput)
        return self

    def choose_manage_shareholder_search_type(self):
        self.wait_and_click(AdvancedSearchObjects.ManagerShareholderInput)
        return self

    def choose_company_search_type(self):
        self.wait_and_click(AdvancedSearchObjects.CompanyInput)
        return self

    def order_report_from_advanced_search(self, report_type: ReportType):
        self.wait_for_element_is_clickable(AdvancedSearchObjects.get_attribute_by_name(report_type.value))
        self.wait_and_click(AdvancedSearchObjects.ConCheckButton)

