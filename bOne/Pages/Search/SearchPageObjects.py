from selenium.webdriver.common.by import By


class SearchPageObjects(object):
    SearchButton = (By.XPATH, '/descendant::button[@class="x-btn-blue-text"][1]')
    InvestigationButton = (By.XPATH, '/descendant::button[@class="x-btn-blue-text"][2]')
    UploadButton = (By.XPATH, '/descendant::button[@class="x-btn-blue-text"][3]')
    AdvancedSearchButton = (By.XPATH, '//input[@id="QueryStringField-input"]/following-sibling::a')  #todo to sprobować złapać jako relatywny element może?
    SearchInput = (By.ID, 'QueryStringField-input')

    SearchResultPlaceholder = (By.XPATH, './/div[@class="x-grid3-body"]/div')
    SearchExpandRecordArrow = (By.XPATH, './/tr/td[2]')
    SearchResultSimilarityTextRelative = (By.XPATH, ".//*[@class='similarity-text']")
    SearchResultHierarchyRelative = (By.XPATH, './/span[contains(@id, "serId_hierarchy_")]')
    SearchResultNameRelative = (By.XPATH, './/span[contains(@id, "serId_name_")]')
    ConfirmButton = (By.XPATH, '//button[contains(@class, "x-btn-orange-text")]')
    FullReportRadio = (By.CSS_SELECTOR, "input[name='FullReport'] + span")
    FullReportGwGRadio = (By.CSS_SELECTOR, 'input[name="FullReportGwG"] + span')
    CreditReportRadio = (By.CSS_SELECTOR, 'input[name="CreditReport"] + span')
    BoniCheckCompactRadio = (By.CSS_SELECTOR, 'input[name="BoniCheckCompact"] + span')
    FinanceCheckRadio = (By.CSS_SELECTOR, 'input[name="FinanceCheck"] + span')
    GwgReportRadio = (By.CSS_SELECTOR, 'input[name="GwGReport"] + span')
    RiskCheckBusinessRadio = (By.CSS_SELECTOR, 'input[name="RiskCheckBusinessBasic"] + span')
    RiskCheckBusinessPlusRadio = (By.CSS_SELECTOR, 'input[name="RiskCheckBusiness"] + span')

    NoMonitoringRadio = (By.XPATH, '//label[text()="No Monitoring"]/following-sibling::span[2]')  #todo dodac id
    MonitoringRadio = (By.XPATH, '//label[text()="Monitoring"]/following-sibling::span[2]')
    MonitoringPlusRadio = (By.XPATH, '//label[text()="Monitoring Plus"]/following-sibling::span[2]')
    MonitoringPlusRadio = (By.XPATH, '//descendant::button[contains(@class, "x-btn-orange-text")][2]')
    AlreadyPurchasedReportButton = (By.XPATH, '//descendant::button[contains(@class, "x-btn-orange-text")][2]')
    PurchaseButton = (By.XPATH, '//descendant::button[contains(@class, "x-btn-orange-text")][3]')


