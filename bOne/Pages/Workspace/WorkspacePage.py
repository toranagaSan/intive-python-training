from selenium.webdriver.remote.webdriver import WebDriver

from Pages.BasePage import BasePage
from Pages.Workspace.WorkspacePageObjects import WorkspacePageObjects as WO


class WorkspacePage(BasePage):

    def __init__(self, driver: WebDriver):
        super(WorkspacePage, self).__init__(driver)
        from Pages.CommonParts.ActionMenu.ActionMenu import ActionMenu
        from Pages.CommonParts.MainMenu.MainMenu import MainMenu
        self._action_menu = ActionMenu(driver)
        self._right_menu = MainMenu(driver)

    def go_to_reports(self):
        self.get_element(WO.ReportsTabBtn).click()

    def go_to_monitoring(self):
        self.get_element(WO.MonitoringTabBtn).click()

    def go_to_monitoring_plus(self):
        self.get_element(WO.MonitoringPlusTabBtn).click()

    def go_to_upload(self):
        self.get_element(WO.UploadTabBtn).click()

    def go_to_archive(self):
        self.get_element(WO.ArchiveBtn).click()

    def go_back_from_archive(self):
        self._driver.forward()

    def export_hitlist(self):
        pass

    def export_report(self):
        pass

    def mark_as_read(self):
        pass

    def mark_as_unread(self):
        pass

    def add_to_archive(self):
        pass

    def add_all_to_archive(self):
        pass

    def switch_between_changed_and_grouped_view(self):
        """
        For Monitoring Plus only tab
        :return:
        """
        pass
