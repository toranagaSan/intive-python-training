from selenium.webdriver.common.by import By


class WorkspacePageObjects(object):
    ReportsTabBtn = (By.XPATH, '/descendant::span[@class="x-tab-strip-inner"][1]')
    MonitoringTabBtn = (By.XPATH, '/descendant::span[@class="x-tab-strip-inner"][2]')
    MonitoringPlusTabBtn = (By.XPATH, '/descendant::span[@class="x-tab-strip-inner"][3]')
    UploadTabBtn = (By.XPATH, '/descendant::span[@class="x-tab-strip-inner"][4]')
    FindBtn = (By.XPATH, '/descendant::button[@class="x-btn-blue-text"][1]')
    ArchiveBtn = (By.XPATH, '/descendant::button[@class="x-btn-blue-text"][2]')
    AdvancedFilterBtn = (By.XPATH, '//i[@class="fa fa-search"]/following-sibling::a')
    FilterInput = (By.ID, 'QueryStringField-input')
