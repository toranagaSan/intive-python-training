import unittest

from parameterized import parameterized

from Assets.ReportType import ReportType
from Pages.Search.AdvancedSearchObjects import AdvancedSearchObjects
from Pages.Search.SearchPageObjects import SearchPageObjects
from Tests.LoginBaseTestCase import LoginBaseTestCase

EMPTY = ""


class SearchTests(LoginBaseTestCase):

    @unittest.skip("Implemented and green")
    def test_ifSingleSearchWorks(self):
        """
        CRIF as input
        :return:
        """
        _input_phrase = 'CRIF'
        self._page.search(_input_phrase)
        _serach_results = self._page.collect_search_results()
        self.assertEqual(_serach_results.__len__(), 33)
        self.assertEqual(_serach_results[0],
                         """1\n  92%\nCompany\nCRIF GmbH\nDessauerstr. 9\n80992\nMünchen\nInactive\nCB number: 21414959, HRB: 143628""")
        self.assertEqual(_serach_results[5],
                         """5\n  90%\nCompany\nCRIF Bürgel GmbH\n25785\nNordhastedt\nActive\nCB number: 62408052""")

    @unittest.skip("Implemented and green")
    def test_ifExpectedElementsAreVisible(self):
        """
        Check for the Search bar
            - Text field for entering the input
            - Search button
            - Investigation button
            - Upload button
            - Link for Advanced search
        :return:
        """
        self.assertEqual(self._page.get_element(SearchPageObjects.SearchInput).is_displayed(), True)
        self.assertEqual(self._page.get_element(SearchPageObjects.SearchButton).is_displayed(), True)
        self.assertEqual(self._page.get_element(SearchPageObjects.InvestigationButton).is_displayed(), True)
        self.assertEqual(self._page.get_element(SearchPageObjects.UploadButton).is_displayed(), True)
        self.assertEqual(self._page.get_element(SearchPageObjects.AdvancedSearchButton).is_displayed(), True)

    @unittest.skip("Implemented and green")
    def test_ifAdvancedMenuButtonsAreVisible(self):
        from Pages.CommonParts.ActionMenu.ActionMenuObjects import ActionMenuObjects
        self._page.open_action_menu()
        self.assertEqual(self._page.get_element(ActionMenuObjects.SearchHistoryButton).is_displayed(), True)
        self.assertEqual(self._page.get_element(ActionMenuObjects.ExportButton).is_displayed(), True)

    @unittest.skip("Not implemented yet")
    def test_ifBusinessCardIsFilledUp(self):
        pass

    @unittest.skip("Implemented and green")
    def test_ifAdvancedSearchIsRenderedCorrectly(self):
        """
        SRC_005
        Check for clicking Advanced search and opening of Advance search page (BCI and BCI + CCI customers)

        SRC_006
        Check for availabiltiy of all the fields in Advanced search section (customizable)
            - Exact match (check box)
            - Name
            - Street and Number
            - Zip code and City
            - Country
            - Date of birth - date picker
            - Telephone
            - E-mail
            - Website
            - Identifier (List and Text)
                - CB number
                - Tax ID number
                - VAT number
                - Registration number
            - Max results - touch based selection
            - Min Similiarity - touch based selection
        :return:
        """
        from Pages.Search.AdvancedSearchObjects import AdvancedSearchObjects as AS
        _expected_elements_list = [AS.ExactMatchInput, \
                                   AS.NameInput, \
                                   AS.StreetInput, \
                                   AS.StreetNumberInput, \
                                   AS.ZipCodeInput, \
                                   AS.CityInput, \
                                   AS.CountryInput, \
                                   AS.DateOfBirthInput, \
                                   AS.TelephoneInput, \
                                   AS.WebsiteInput, \
                                   AS.IdentifierDropdown, \
                                   AS.IdentifierInput, \
                                   AS.CloseButton, \
                                   AS.ClearButton, \
                                   AS.SearchButton, \
                                   # AS.MaxResults, \
                                   # AS.MinSimilarity
                                   ]
        self.assertEqual(self._page.open_advanced_search().are_elements_visible(_expected_elements_list), True, \
                         "Not all expected elements are visible in advanced search form")

    @unittest.skip("Implemented and green")
    def test_ifSearchTypeAndFiltersSectionsVisibleInAdvancedSearch(self):
        from Pages.Search.AdvancedSearchObjects import AdvancedSearchObjects as AS
        _expected_elements_list = [AS.ExactMatchInput, \
                                   AS.CompanyInput, \
                                   AS.ManagerShareholderInput, \
                                   AS.ConsumerInput, \
                                   AS.HeadOfficeInput, \
                                   AS.BranchInput, \
                                   AS.ActiveInput, \
                                   AS.InactiveInput, \
                                   ]
        self.assertEqual(self._page.open_advanced_search().are_elements_visible(_expected_elements_list), True, \
                         "Not all expected elements are visible in advanced search/ type and filters section")

    # @unittest.skip("Not implemented yet")
    def test_ifDirectProductCallIsVisibleInAdvancedSearch(self):
        self._page.open_advanced_search()
        self.assertEqual(self._page.get_element(AdvancedSearchObjects.DpcSection).is_displayed(), True)

    # @unittest.skip("Not implemented yet")
    @parameterized.expand([({AdvancedSearchObjects.NameInput: "Andrea Gomoll-Wünsche",
                             AdvancedSearchObjects.StreetInput: EMPTY,
                             AdvancedSearchObjects.ZipCodeInput: EMPTY,
                             AdvancedSearchObjects.CityInput: EMPTY
                             }),
                           ({AdvancedSearchObjects.NameInput: "Andrea Gomoll-Wünsche",
                             AdvancedSearchObjects.StreetInput: "Dallgower Str.",
                             AdvancedSearchObjects.StreetNumberInput: EMPTY,
                             AdvancedSearchObjects.ZipCodeInput: "14612",
                             AdvancedSearchObjects.CityInput: EMPTY
                             })
                           ])
    def test_ifDirectProductCallValidateForms(self, input_data):
        self._page.open_advanced_search()
        self._page.fill_up_advanced_search_form(input_data) \
            .choose_manage_shareholder_search_type() \
            .order_report_from_advanced_search(ReportType.RiskCheckBusiness)
        from Pages.Report.ReportPage import ReportPage
        return ReportPage(self._driver).is_report_page_loaded()
        self.assertEqual(False, True)

    @unittest.skip("Not implemented yet")
    def test_ifUserCanOrdeRiskCheckBusinessViaDpc(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifOrderingWindowIsProperlyRendered(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanCleanAdvancedSearch(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanCloseAdvancedSearchWindow(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifSearchReturnsAppropriateObjects(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterByHeadOffice(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterByBranch(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterByActive(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterByInactive(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsBySimilarity(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsByHierarchy(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsByHitType(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsByName(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsByStreet(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsByZipCode(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsByCity(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsByStatus(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsByIdentifier(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_ifUserCanFilterResultsByWebsite(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_searchHistory(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_exportHitlist(self):
        pass

    @unittest.skip("Not implemented yet")
    def test_exportReport(self):
        pass

    @parameterized.expand([("44013677",), ("11430194",)])
    def test_purchasingFullReport(self, onr):
        self._page.search(onr)
        self._page.purchase_full_report()
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingFullReportGwg(self, onr):
        self._page.search(onr)
        self._page.purchase_full_report_gwg()
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingCreditReport(self, onr):
        self._page.search(onr)
        self._page.purchase_credit_report()
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingBoniCheckCompact(self, onr):
        self._page.search(onr)
        self._page.purchase_boni_check_report()
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingFinancialCheck(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report()
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingGwgReport(self, onr):
        self._page.search(onr)
        self._page.purchase_gwg_report()
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingRiskCheckBusiness(self, onr):
        self._page.search(onr)
        self._page.purchase_risk_check_report()
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingFullReportWithMonitoring(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=False)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingFullReportGwgWithMonitoring(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=False)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingCreditReportWithMonitoring(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=False)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingBoniCheckCompactWithMonitoring(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=False)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingFinancialCheckWithMonitoring(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=False)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingGwgReportWithMonitoring(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=False)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([("44013677",)])
    def test_purchasingRiskCheckBusinessWithMonitoring(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=False)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([(44013677,)])
    def test_purchasingFullReportWithMonitoringPlus(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=True)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([(44013677,)])
    def test_purchasingFullReportGwgWithMonitoringPlus(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=True)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([(44013677,)])
    def test_purchasingCreditReportWithMonitoringPlus(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=True)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([(44013677,)])
    def test_purchasingBoniCheckCompactWithMonitoringPlus(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=True)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([(44013677,)])
    def test_purchasingFinancialCheckWithMonitoringPlus(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=True)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([(44013677,)])
    def test_purchasingGwgReportWithMonitoringPlus(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=True)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([(44013677,)])
    def test_purchasingRiskCheckBusinessWithMonitoringPlus(self, onr):
        self._page.search(onr)
        self._page.purchase_finance_check_report(monitoring=True, plus=True)
        self.assertEqual(self._page.is_report_rendered(), True)

    @parameterized.expand([{AdvancedSearchObjects.NameInput: "Andrea Gomoll-Wünsche",
                            AdvancedSearchObjects.StreetInput: "Dallgower Str.",
                            AdvancedSearchObjects.StreetNumberInput: "Dallgower Str.",
                            AdvancedSearchObjects.ZipCodeInput: "14612",
                            AdvancedSearchObjects.CityInput: "Falkensee"
                            }])
    def test_purchasingConCheck(self, input_data):
        self._page.open_advanced_search()
        self._page.fill_up_advanced_search_form(input_data) \
            .choose_consumer_search_type() \
            .order_report_from_advanced_search(ReportType.ConCheck)

    @parameterized.expand([{AdvancedSearchObjects.NameInput: "Andrea Gomoll-Wünsche",
                            AdvancedSearchObjects.StreetInput: "Dallgower Str.",
                            AdvancedSearchObjects.StreetNumberInput: "Dallgower Str.",
                            AdvancedSearchObjects.ZipCodeInput: "14612",
                            AdvancedSearchObjects.CityInput: "Falkensee"
                            }])
    def test_purchasingConCheckPlusIdent(self, input_data):
        self._page.open_advanced_search()
        self._page.fill_up_advanced_search_form(input_data) \
            .choose_consumer_search_type() \
            .order_report_from_advanced_search(ReportType.ConCheckPlusIdent)

    @parameterized.expand([{AdvancedSearchObjects.NameInput: "Andrea Gomoll-Wünsche",
                            AdvancedSearchObjects.StreetInput: "Dallgower Str.",
                            AdvancedSearchObjects.StreetNumberInput: "Dallgower Str.",
                            AdvancedSearchObjects.ZipCodeInput: "14612",
                            AdvancedSearchObjects.CityInput: "Falkensee"
                            }])
    def test_purchasingNegativeCheck(self, input_data):
        self._page.open_advanced_search()
        self._page.fill_up_advanced_search_form(input_data) \
            .choose_consumer_search_type() \
            .order_report_from_advanced_search(ReportType.NegativeCheck)

    @parameterized.expand([{AdvancedSearchObjects.NameInput: "Andrea Gomoll-Wünsche",
                            AdvancedSearchObjects.StreetInput: "Dallgower Str.",
                            AdvancedSearchObjects.StreetNumberInput: "Dallgower Str.",
                            AdvancedSearchObjects.ZipCodeInput: "14612",
                            AdvancedSearchObjects.CityInput: "Falkensee"
                            }])
    def test_purchasingNegativeCheckHard(self, input_data):
        self._page.open_advanced_search()
        self._page.fill_up_advanced_search_form(input_data) \
            .choose_consumer_search_type() \
            .order_report_from_advanced_search(ReportType.NegativeCheckHard)
    #
    # def test_(self):
    #     pass
    #
    # def test_(self):
    #     pass
    #
    # def test_(self):
    #     pass
    #
    # def test_(self):
    #     pass
    #
    # def test_(self):
    #     pass
    #
    # def test_(self):
    #     pass
    #
    # def test_(self):
    #     pass
    #
    # def test_(self):
    #     pass
    #
    # def test_(self):
    #     pass
    #
    # def test_(self):
    #     pass
