import logging
import random

from Tests.BaseTestCase import BaseTestCase


class LoginTests(BaseTestCase):

    def test_boneLogoIsVisible(self):
        logging.info("Test test_boneLogoIsVisible")
        from Pages.Login.LoginPageObjects import LoginPageObjects
        self.assertEqual(self._page.get_element(LoginPageObjects.MainLogo).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.HomeLink).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.ServicesLink).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.AboutUsLink).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.ContactLink).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.ForgotYourPasswordLink).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.PrivacyPolicyLink).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.TermsConditionsLink).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.ImprintLink).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.NewsletterLink).is_displayed(), True)
        self.assertEqual(self._page.get_element(LoginPageObjects.TomLink).is_displayed(), True)

    def test_isLanguageTogglesClickable(self):
        """
        Will raise an error when elements will be not visble
        :return:
        """
        logging.info("Test test_isLanguageTogglesClickable")
        self._page.change_language_to_german()
        self._page.change_language_to_english()

    def test_successfulLogin(self):
        logging.info("Test successfulLogin")
        _logged_in_user_details = self._page. \
            login_with_valid_user(self._env_settings.basic_user). \
            get_login_user_details()
        self.assertEqual("Pawel Tarsa", _logged_in_user_details["user_name"])
        self.assertEqual("ID: 27332", _logged_in_user_details["id"])
        self.assertEqual("BÜRGEL M", _logged_in_user_details["member"])

    def test_wrongLogin(self):
        _user = self._env_settings.basic_user
        _user.login += random.choice('asdf1324')
        self._page.implicit_wait(10)
        _page_url = self._page.login(_user) \
            .get_page_url()
        self.assertEqual("https://test-bci.crifbuergel.de/rightsError", _page_url)

    def test_wrongPassword(self):
        _user = self._env_settings.basic_user
        _user.password += random.choice('asdf1324')
        _message_text = self._page. \
            login_with_invalid_user(_user)
        self.assertEqual("The combination of user ID and password is invalid.", _message_text)
