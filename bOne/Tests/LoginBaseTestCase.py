from Pages.Login.LoginPage import LoginPage
from Tests.BaseTestCase import BaseTestCase


class LoginBaseTestCase(BaseTestCase):

    def setUp(self, browser='chrome', env='test'):
        super(LoginBaseTestCase, self).setUp(browser, env)
        from Helpers.Factories.SettingsFactory import SettingsFactory
        self._page = LoginPage(self._bone_driver).login(SettingsFactory.get_settings(env).basic_user)
