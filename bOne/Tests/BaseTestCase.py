import unittest
import logging
from datetime import datetime
import sys

from selenium.webdriver.support.ui import WebDriverWait

from Pages.Login.LoginPage import LoginPage


class BaseTestCase(unittest.TestCase):
    WAIT_TIME = 30
    SCREENSHOTS_PATH = "C:/Tests/Reports/"

    def setUp(self, browser='chrome', env='test'):
        from Settings.setup_utils import setup_logging
        from Helpers.Factories.SettingsFactory import SettingsFactory
        from Helpers.Factories.WebdriverFactory import WebdriverFactory
        setup_logging()
        self._env_settings = SettingsFactory.get_settings(env)
        self._bone_driver = WebdriverFactory.get_webdriver(browser)
        self._bone_driver.maximize_window()
        self._bone_driver.get(self._env_settings.url)
        self._wait = WebDriverWait(self._bone_driver, BaseTestCase.WAIT_TIME)
        self._page = LoginPage(self._bone_driver)
        # todo temporary
        self._page.change_language_to_english()

    def tearDown(self):
        if sys.exc_info()[0]:
            _tc_name = self._testMethodName
            _creation_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            logging.info(f"Making screenshot from {_tc_name}...")
            self._bone_driver.get_screenshot_as_file(f"./Reports/Screenshots_{_tc_name}_{_creation_time}.jpg")
        self._bone_driver.quit()
