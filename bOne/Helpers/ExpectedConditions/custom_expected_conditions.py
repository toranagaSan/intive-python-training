from selenium.webdriver.support.expected_conditions import visibility_of_element_located


class element_is_selectable(object):
    """
    An Expectation for checking an button ''
    enabled:
    <button class="x-btn-orange-text " type="button" style="position: relative; height: 24px; width: 85px;" tabindex="0" aria-disabled="true">Confirm</button>
    disabled:
    <button class="x-btn-orange-text " type="button" style="position: relative; height: 24px; width: 85px;" tabindex="0" aria-disabled="false">Confirm</button>
    """
    def __init__(self, locator):
        self.locator = locator

    def __call__(self, driver):
        element = visibility_of_element_located(self.locator)(driver)
        if element and element.get_attribute('aria-disabled') == 'false':
            return element
        else:
            return False
