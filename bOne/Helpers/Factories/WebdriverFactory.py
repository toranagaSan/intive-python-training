import selenium.webdriver as webdriver
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary


class WebdriverFactory(object):

    @staticmethod
    def get_webdriver(browser_name):
        browser_name = browser_name.lower()
        if browser_name == 'firefox':
            binary = FirefoxBinary('./Settings/resources/geckodriver.exe')
            return webdriver.Firefox(firefox_binary=binary)
        elif browser_name == 'chrome':
            return webdriver.Chrome(
                executable_path="C:\\Users\\pl067pawtars\\PycharmProjects\\bOne\\Settings\\resources\\chromedriver.exe")
        elif browser_name == 'ie':
            return webdriver.Ie()
        raise Exception("No such '{0}' browser exists".format(browser_name))
