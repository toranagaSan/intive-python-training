from Settings.TestSettings import TestSettings


class SettingsFactory(object):

    @staticmethod
    def get_settings(env):
        if env == 'test':
            return TestSettings()
        raise Exception("No such '{0}' env exists".format(env))
